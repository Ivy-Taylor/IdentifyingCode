package com.example.lenovo.identifyingcode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private  BroadcastReceiver receiver;
    private EditText et_code;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_code = (EditText) findViewById(R.id.et_code);

        //  动态注册广播接收者
        receiver = new CodeReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.ivy.getCode");
        registerReceiver(receiver,filter);
    }
    private class CodeReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String code = intent.getStringExtra("code");
            et_code.setText(code);
        }
    }

    /**
     * 必须注销，必须注销，必须注销！！
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
