package com.example.lenovo.identifyingcode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.telephony.SmsMessage;

/**
 * Created by Lenovo on 2017/10/21.
 */

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //  pdu：protocol data unit
        Object[] objects = (Object[]) intent.getExtras().get("pdus");
        for (Object obj : objects) {
            //  创建短信的消息对象
            SmsMessage message = SmsMessage.createFromPdu((byte[]) obj);
            //  获取短信的发送者
            String from = message.getOriginatingAddress();
            //  获取消息的内容
            String messageBody = message.getMessageBody();
            //      判断究竟是谁发的 规定12345是自己发的
            if("12345".equals(from)) {
                //  发送广播到通知界面  把短信的内容设置到editText里
                Intent service = new Intent();
                service.setAction("com.ivy.getCode");
                service.putExtra("code", messageBody);
                context.sendBroadcast(intent);
            }
        }
    }
}
